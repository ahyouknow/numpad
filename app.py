import tkinter as tk
from tkinter import *

class dopeButton(tk.Button):
    def __init__(self, master, **kw):
        tk.Button.__init__(self,master=master, relief=RAISED, font=('Droid Sans Fallback', '25', 'bold'), **kw)
        self['fg'] = 'white'
        self['background'] = '#222'
        self['activeforeground'] = 'white'
        self['activebackground'] = '#222'
        self.bind("<ButtonPress>", self.ButtonPress)
        self.bind("<ButtonRelease>", self.ButtonRelease)

    def ButtonPress(self, e):
        self['fg'] = 'green'
        self['activeforeground'] = 'green'
        self['background'] = '#1E1E1E'
        self['activebackground'] = '#1E1E1E'

    def ButtonRelease(self, e):
        self['fg'] = 'white'
        self['activeforeground'] = 'white'
        self['background'] = '#222'
        self['activebackground'] = '#222'

class FullScreenApp(tk.Frame):
    padding=3
    dimensions="{0}x{1}+0+0"

    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master)
        self.master=master
        width=master.winfo_screenwidth()-self.padding
        height=master.winfo_screenheight()-self.padding
        master.geometry(self.dimensions.format(width, height))
        row = -1
        column = 0
        self.buttons = {}
        for x in range(1,11):
            if column == 0:
                row+=1
            number = x%10
            self.buttons[number] = dopeButton(self.master, text=str(number), command=lambda returnNumber = number: self.pressed(returnNumber))
            self.buttons[number].grid(row=row, column=column, columnspan=1, sticky='nsew')
            column = (column+1)%3
        self.buttons['go'] = dopeButton(self.master, text='Go', command=lambda: self.pressed('go'))
        self.buttons['go'].grid(row=3, column=1, columnspan=2, sticky='nsew')

    def pressed(self, number):
        print(number)

root=tk.Tk()
root.wm_attributes('-fullscreen','true')

FullScreenApp(root)

root.grid_rowconfigure((0,1,2,3), weight=1)
root.grid_columnconfigure((0,1,2), weight=1)
root.configure(background='black')

root.mainloop()

